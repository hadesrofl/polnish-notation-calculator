package Calculator.Symbols


sealed trait CalcToken
case class CalcTerm(term: Double) extends CalcToken

sealed abstract class CalcOperator(val op: String = "", val precedence: Int = 0, val associativity: String = "") extends CalcToken {
  def calculate(x: Double, y: Double): Double
}

case class Addition(override val op: String = MathSymbol.Plus.op, override val precedence: Int = 1, override val associativity: String = Associativity.Left.toString) extends CalcOperator{
  override def calculate(x: Double, y: Double): Double = x + y
}

case class Subtraction(override val op: String = MathSymbol.Minus.op, override val precedence: Int = 1, override val  associativity: String = Associativity.Left.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = x - y
}

case class Multiplication(override val op: String = MathSymbol.Mul.op, override val precedence: Int = 2, override val associativity: String = Associativity.Left.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = x * y
}

case class Division(override val op: String = MathSymbol.Div.op, override val precedence: Int = 2, override val associativity: String = Associativity.Left.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = x / y
}

case class Power(override val op: String = MathSymbol.Pow.op, override val precedence: Int = 3, override val associativity: String = Associativity.Right.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = Math.pow(x,y)
}

case class LeftParen(override val op: String = MathSymbol.LeftParen.op, override val precedence: Int = 4, override val associativity: String = Associativity.Left.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = ???
}

case class RightParen(override val op: String = MathSymbol.RightParen.op, override val precedence: Int = 4, override val associativity: String = Associativity.Left.toString) extends CalcOperator {
  override def calculate(x: Double, y: Double): Double = ???
}
