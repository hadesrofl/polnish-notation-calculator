package Calculator.Symbols

sealed abstract class MathSymbol(val op: String)

object MathSymbol {
  case object Plus extends MathSymbol("+")
  case object Minus extends MathSymbol("-")
  case object Mul extends MathSymbol("*")
  case object Div extends MathSymbol("/")
  case object Pow extends MathSymbol("^")
  case object LeftParen extends MathSymbol("(")
  case object RightParen extends MathSymbol(")")

}
