package Calculator

import Calculator.Symbols._

class Calculator {
  def printToken(token: CalcToken): Any = token match {
    case CalcTerm(term) => print(term)
    case Addition(op, _, _) => print(op)
    case Subtraction(op, _, _) => print(op)
    case Multiplication(op, _, _) => print(op)
    case Division(op, _, _) => print(op)
    case LeftParen(op, _, _) => print(op)
    case RightParen(op, _, _) => print(op)
    case Power(op, _, _) => print(op)
  }

  def evaluatePolishNotation(tokens: List[CalcToken]): Double = {
    var resultStack = List[Double]()
    for (t <- tokens) {
      t match {
        case CalcTerm(term) => resultStack = resultStack.+:(term)
        case co: CalcOperator => {
          val iterator: Iterator[Double] = resultStack.toIterator
          val y: Double = iterator.next()
          val x: Double = iterator.next()
          resultStack = resultStack.drop(2)
          resultStack = resultStack.+:(co.calculate(x, y))
        }
      }
    }
    resultStack.head
  }

  def reversePolish(input: String): List[CalcToken] = {
    def isTerm(token: String): Boolean = {
      token.matches("\\d+(.|,)\\d+|\\d+")
    }

    def identifyOperator(input: String): CalcOperator = {
      var out: CalcOperator = null
      if (input.equalsIgnoreCase(MathSymbol.Plus.op)) out = Addition()
      else if (input.equalsIgnoreCase(MathSymbol.Minus.op)) out = Subtraction()
      else if (input.equalsIgnoreCase(MathSymbol.Mul.op)) out = Multiplication()
      else if (input.equalsIgnoreCase(MathSymbol.Div.op)) out = Division()
      else if (input.equalsIgnoreCase(MathSymbol.LeftParen.op)) out = LeftParen()
      else if (input.equalsIgnoreCase(MathSymbol.RightParen.op)) out = RightParen()
      else if (input.equalsIgnoreCase(MathSymbol.Pow.op)) out = Power()
      out
    }

    def shuntingYard(input: String): List[CalcToken] = {
      var outputQueue = List[CalcToken]()
      var operatorStack = List[CalcOperator]()
      // read tokens
      for (c <- input.split("[^\\d+(.|,)\\d+|\\d+|(\\(|\\+|\\-|\\*|/|^|\\))]")) {
        var token: CalcToken = null
        var operator: CalcOperator = null
        if (isTerm(c.toString)) token = CalcTerm(c.toString.replace(",", ".").toDouble)
        else operator = identifyOperator(c.toString)
        if (token != null) outputQueue = outputQueue.+:(token)
        else if (operator != null) {
          // if operator is "(" put it onto stack
          if (operatorStack.isEmpty || operator.op == MathSymbol.LeftParen.op)
            operatorStack = operatorStack.+:(operator)

          // else if operator is ")" pop operato   asdasdadwars until you hit onto a "("
          else if (operator.op == MathSymbol.RightParen.op) {
            while (operatorStack.nonEmpty && operatorStack.head.op != MathSymbol.LeftParen.op) {
              outputQueue = outputQueue.+:(operatorStack.head)
              operatorStack = operatorStack.drop(1)
            }
            // if found a "(" discard it
            if (operatorStack.head.op == MathSymbol.LeftParen.op)
              operatorStack = operatorStack.drop(1)
          }

          // if the operator is not "(" or ")" just push it to the stack if precedence is ok
          else {
            while (operatorStack.nonEmpty && operatorStack.head.op != MathSymbol.LeftParen.op &&
              (operatorStack.head.precedence > operator.precedence || operatorStack.head.precedence == operator.precedence && operatorStack.head.associativity == Associativity.Left.toString)) {
              outputQueue = outputQueue.+:(operatorStack.head)
              operatorStack = operatorStack.drop(1)
            }
            operatorStack = operatorStack.+:(operator)
          }
        }
      }
      (operatorStack.reverse ::: outputQueue).reverse
    }

    val outputQueue = shuntingYard(input)
    outputQueue
  }
}
