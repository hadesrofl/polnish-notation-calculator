import Calculator.Calculator

object Launcher extends App {
  val calculator = new Calculator()
  //    val input = "( 13.5 + ( 4.3 - 5.7 ) ) * 6.8 / ( 7.9 ^ 8.12 )"
  var answer: String = ""
  do {
    println("Please enter the equation you want to solve (use whitespaces for each term / operator, e.g. '( 13.5 + ( 4.3 - 5.7 ) ) * 6.8' ):")
    val input = scala.io.StdIn.readLine()
    val polishNotation = calculator.reversePolish(input)
    println("Input (Infix): " + input + "\n")
    print("Input (Reverse Polish Notation): ")
    polishNotation.foreach(n => {
      calculator.printToken(n)
      print(" ")
    })
    println("\n")
    println("Result: " + calculator.evaluatePolishNotation(polishNotation))
    println("Do you want to calculate another equation?")
    answer = scala.io.StdIn.readChar().toString
  } while (answer.equalsIgnoreCase("y"))
}

